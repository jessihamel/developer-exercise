class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    new_str = str.gsub(/[\w']+/) do |word|
      if word.length > 4 && word == word.capitalize
        word = "Marklar"
      elsif word.length > 4
        word = "marklar"
      else
        word = word
      end
    end
    new_str
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    fibonaccis = [1, 1]
    (nth-2).times do
      fibonaccis << (fibonaccis[-1] + fibonaccis[-2])
    end
    evens = fibonaccis.map do |number|
      number if number % 2 == 0
    end
    sum = 0
    evens.compact.each do |number|
      sum = sum + number
    end
    return sum
  end

end
